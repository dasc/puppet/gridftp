class gridftp (
    String $service_ensure,
    Boolean $service_enable,
    String $service_name,
    String $service_package_name,
    Integer $port,
    Array[Enum['ERROR', 'WARN', 'INFO', 'TRANSFER', 'DUMP', 'ALL']] $log_level,
    String $log_single,
    String $log_transfer,
    Array[Struct[{name => String,
                  value => String,
		  Optional[as_env] => Boolean}]] $other_options,
) {
    package {$service_package_name: ensure => 'installed', }

    service {$service_name:
        ensure => $service_ensure,
        enable => $service_enable,
	require => Package[$service_package_name],
    }

    file { '/var/log/gridftp': 
	ensure => directory,
	owner => 'root',
	group => 'root',
	mode => '0755',
	require => Package[$service_package_name],
    }

    file {'/etc/gridftp.d':
        ensure => 'directory',
	owner => 'root',
	group => 'root',
	mode => '0755',
	require => Package[$service_package_name],
    }

    file {'/etc/gridftp.conf':
        ensure => 'file',
	owner => 'root',
	group => 'root',
	mode => '0644',
	content => epp('gridftp/gridftp.conf.epp', {
	    port => $port,
	    log_level => $log_level,
	    log_single => $log_single,
	    log_transfer => $log_transfer,
	    other_options => $other_options,
	}),
	require => Package[$service_package_name],
	notify => Service[$service_name],
    }
}
